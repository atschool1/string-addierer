﻿using System;

namespace Zeichenketten_addieren
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This programm adds to binary numbers together \n");
            Console.WriteLine("Write first number:");
            string number01 =Console.ReadLine();
            Console.WriteLine("Write second number:");
            string number02 = Console.ReadLine();

            #region make both numbers the same Length
            int length = number01.Length - number02.Length;
            if (length > 0)
            {
                for (int i = length; i > 0; i--)
                {
                    number02 = "0" + number02;
                }
            }
            else if(length < 0)
            {
                length = length * -1;
                for (int i = length; i > 0; i--)
                {
                    number01 = "0" + number01;
                }
            }
            #endregion

            #region addNumbers
            string result = "";
            bool ci = false;
            for (int i = number01.Length -1 ; 0 <= i; i--)
            {
                bool x = charToBoolean(number01[i]);
                bool y = charToBoolean(number02[i]);

               var nextDigit = (x && !y && !ci) || (!x && y && !ci) || (!x && !y && ci) || (x && y && ci);
               result = result.Insert(0, Convert.ToString(Convert.ToInt32(nextDigit)));

                ci = (x && y && !ci) || (x && !y && ci) || (!x && y && ci) || (x && y && ci);
            }
            if (ci == true)
            {
                result = result.Insert(0, "1");
            }
            Console.WriteLine("The result is:" + result);

            bool charToBoolean(char number)
            {
                if (number == '1')
                {
                    return true;
                }
                return false;
            }
            #endregion
        }
    }
}
